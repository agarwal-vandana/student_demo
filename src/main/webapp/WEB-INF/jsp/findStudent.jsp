<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Search Student</title>
        <%@ include file="head.jsp" %>
    </head>
    <body>
        <h2 class="text-center p-25">Search Student</h2>
        <form id="searchStudentForm" class="text-center">
            <label>Student ID: </label>
            <input type="text" id="studentId" name="studentId" />
            <button type="button" id="searchStudent">Search</button>
        </form>
        <div id="studentDetails" class="text-center" style="display: none;">
            <%@ include file="studentDetails.jsp" %>
        </div>
        <a href="/student-demo/home" class="back-to-home">Back to Home</a>
        <script type="text/javascript">
            $(function() {
                $("#searchStudent").click(function() {
                    if($("#studentId").val().length > 0) {
                        var searchStudentURL = "/student-demo/getStudent";
                        $.post(searchStudentURL, $( "#searchStudentForm" ).serialize())
                            .done(function(data) {
                                if(data != "") {
                                    $("#student-id").text(data.id);
                                    $("#student-name").text(data.name);
                                    $("#student-class").text(data.studentClass);
                                    $("#student-roll-no").text(data.rollNumber);
                                    $("#student-age").text(data.age);
                                    $("#student-dob").text(data.dob);
                                    $("#student-picture").attr("src", data.studentPicture);
                                    $("#student-picture").attr("alt", data.name);

                                    $("#studentDetails").show();
                                } else {
                                    $("#studentDetails").hide();
                                    alert("No Record Found!");
                                }
                            })
                            .fail(function(data) {
                                $("#studentDetails").hide();
                                alert("Cannot search the student!");
                            });
                    } else {
                        alert("Please input Student ID");
                    }

                });
            });
        </script>
    </body>
</html>
