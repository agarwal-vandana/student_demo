<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.demo.student.entity.Student" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Students List</title>
        <%@ include file="head.jsp" %>
    </head>
    <body>
        <h2 class="text-center p-25">Students List</h2>
        <div class="student-list-container">
            <table id="studentList" border="1" align="center">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Student Name</th>
                    <%--<th>Class</th>
                    <th>Roll No.</th>
                    <th>Age</th>
                    <th>DOB</th>--%>
                    <th>Student Picture</th>
                </tr>
                </thead>
                <tbody>
                <% List students = (ArrayList)request.getAttribute("students");
                    for (int i = 0; i < students.size(); i++) {
                        Student student = (Student) students.get(i); %>
                <tr>
                    <td><%= student.getId() %></td>
                    <td>
                        <a class="student-link" data-toggle="modal" data-target = "#student-detail-dialog">
                            <%= student.getName() %>
                        </a>
                    </td>
                    <%--<td><%= student.getStudentClass() %></td>
                    <td><%= student.getRollNumber() %></td>
                    <td><%= student.getAge() %></td>
                    <td><%= student.getDOB() %></td>--%>
                    <td>
                        <a data-toggle="modal" data-target = "#student-detail-dialog">
                            <img src="<%= student.getStudentPicture()%>" alt="<%= student.getName()%>" />
                        </a>
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>
        </div>

        <div id="student-detail-dialog" class="modal fade"
             tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><strong>Student Details</strong></h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close" id="close-student-details-dialog">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-3 text-center">
                                <img id="student-picture" src="" alt="" />
                            </div>
                            <div class="col-sm-9">
                                <label class="label-control">Id: </label> <span id="student-id"></span> <br/>
                                <label class="label-control">Name: </label> <span id="student-name"></span> <br/>
                                <label class="label-control">Class: </label> <span id="student-class"></span> <br/>
                                <label class="label-control">Roll No.: </label> <span id="student-roll-no"></span> <br/>
                                <label class="label-control">Age: </label> <span id="student-age"></span> <br/>
                                <label class="label-control">DOB: </label> <span id="student-dob"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="close-student-detail-dialog" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#student-detail-dialog">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <a href="/student-demo/home" class="back-to-home">Back to Home</a>

        <script type="text/javascript">
            $(function() {
                // When the dialog shows add the student data
                $('#student-detail-dialog').on('show.bs.modal', function (event) {

                });
            });
        </script>
    </body>
</html>
