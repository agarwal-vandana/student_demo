<html>
    <link>
        <title>Home</title>
        <%@ include file="head.jsp" %>
    </head>
    <body>
        <h2 class="text-center p-25">Student Demo Application</h2>
        <ul>
            <li><a href="/student-demo/getAllStudents">View All Students</a></li>
            <li><a href="/student-demo/searchStudent">Find Student</a></li>
        </ul>
    </body>
</html>
