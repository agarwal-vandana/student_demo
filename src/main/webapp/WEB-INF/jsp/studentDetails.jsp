<h5>Search Results:</h5>
<table border="1" align="center">
    <thead>
    <tr>
        <th>Student Id</th>
        <th>Student Name</th>
        <th>Class</th>
        <th>Roll No.</th>
        <th>Age</th>
        <th>DOB</th>
        <th>Student Picture</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><span id="student-id"></span></td>
        <td><span id="student-name"></span></td>
        <td><span id="student-class"></span></td>
        <td><span id="student-roll-no"></span></td>
        <td><span id="student-age"></span></td>
        <td><span id="student-dob"></span></td>
        <td><img id="student-picture" src="" alt="" /></td>
    </tr>
    </tbody>
</table>
