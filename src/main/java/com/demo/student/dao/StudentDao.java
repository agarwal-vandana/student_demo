package com.demo.student.dao;

import com.demo.student.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentDao {

    @Autowired
    protected HibernateTemplate hibernateTemplate;

    public List<Student> getAllStudents() {

        return hibernateTemplate.loadAll(Student.class);

    }

    public Student getStudentById(Long studentId) {

        return hibernateTemplate.get(Student.class, studentId);

    }
}
