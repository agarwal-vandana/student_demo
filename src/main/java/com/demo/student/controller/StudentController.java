package com.demo.student.controller;

import com.demo.student.entity.Student;
import com.demo.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/student-demo")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/home")
    public String displayHome() {
        return "index";
    }

    @GetMapping(value="/searchStudent")
    public String displaySearchStudent() {
        return "findStudent";
    }

    @GetMapping(value="/getAllStudents")
    public String getAllStudents(HttpServletRequest request, HttpServletResponse response) {

        List<Student> students = new ArrayList<>();

        try {
            students = studentService.getAllStudents();
        } catch(Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("students", students);

        return "studentList";
    }

    @RequestMapping(value = "/getStudent", method = RequestMethod.POST)
    public @ResponseBody Student getStudentById(@RequestParam(value = "studentId") Long studentId) {

        if(studentId == null) {
            return null;
        }

        Student student = new Student();

        try {
            student = studentService.getStudentById(studentId);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return student;
    }

}
