package com.demo.student.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages = "com.demo.student")
@PropertySource(value = { "classpath:db.properties" })
public class AppConfig extends WebMvcConfigurerAdapter {

    @Value("${jdbc.database.driver.class.name}")
    public String DB_DRIVER;

    @Value("${jdbc.database.url}")
    public String DB_URL;

    @Value("${jdbc.database.schema}")
    public String DB_SCHEMA;

    @Value("${jdbc.database.username}")
    public String DB_USER;

    @Value("${jdbc.database.password}")
    public String DB_PASSWORD;

    @Value("${jdbc.database.maxPoolSize}")
    public int DB_MAX_POOL_SIZE;

    @Value("${jdbc.database.minPoolSize}")
    public int DB_MIN_POOL_SIZE;

    @Value("${hibernate.dialect}")
    public String HIBERNATE_DIALECT;

    @Value("${hibernate.show.sql}")
    public String HIBERNATE_SHOW_SQL;

    @Value("${hibernate.hbm2ddl.auto}")
    public String HIBERNATE_HBM2DDL_AUTO;

    @Value("${hibernate.package.to.scan}")
    public String HIBERNATE_PACKAGE_TO_SCAN;

    @Value("${jdbc.connection.pool.enabled}")
    public Boolean JDBC_CONNECTION_POOL_ENABLED;

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }

    @Bean
    public DataSource dataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(DB_DRIVER);
        dataSource.setUrl(DB_URL + "/" + DB_SCHEMA);
        dataSource.setUsername(DB_USER);
        dataSource.setPassword(DB_PASSWORD);

        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean getSessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        sessionFactoryBean.setPackagesToScan(HIBERNATE_PACKAGE_TO_SCAN);
        sessionFactoryBean.setHibernateProperties(getHibernateProperties());
        sessionFactoryBean.setPhysicalNamingStrategy(new DaoSQLImprovedNamingStrategy());

        return sessionFactoryBean;
    }

    @Bean
    public Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.default_schema", "dbo");
        properties.put("hibernate.dialect", HIBERNATE_DIALECT);
        properties.put("hibernate.show_sql", HIBERNATE_SHOW_SQL);
        properties.put("hibernate.hbm2ddl.auto", HIBERNATE_HBM2DDL_AUTO);
        properties.put("hibernate.physical_naming_strategy", DaoSQLImprovedNamingStrategy.class.getPackage().getName() + "." + DaoSQLImprovedNamingStrategy.class.getSimpleName());
        properties.put("hibernate.enable_lazy_load_no_trans", "true");

        return properties;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }

    @Bean
    public HibernateTemplate hibernateTemplate(SessionFactory sessionFactory) {
        HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
        hibernateTemplate.setCheckWriteOperations(false);
        return hibernateTemplate;
    }

    @Bean
    public InternalResourceViewResolver setupViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

}
