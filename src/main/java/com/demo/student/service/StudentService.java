package com.demo.student.service;

import java.util.List;
import com.demo.student.entity.Student;

public interface StudentService {

    List<Student> getAllStudents();

    Student getStudentById(Long studentId);

}
