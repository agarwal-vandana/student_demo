package com.demo.student.service.impl;

import com.demo.student.dao.StudentDao;
import com.demo.student.entity.Student;
import com.demo.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Transactional
    public List<Student> getAllStudents() {
        System.out.println("in service");
        return studentDao.getAllStudents();

    }

    @Transactional
    public Student getStudentById(Long studentId) {

        return studentDao.getStudentById(studentId);

    }
}
